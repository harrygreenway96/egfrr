/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Validator;
import java.time.LocalDate;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author k1423138
 */
public class ValidatorTest {
    
    public ValidatorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of isValidNHSID method, of class Validator.
     */
    @Test
    public void testIsValidNHSID() {
        System.out.println("isValidNHSID");
        String valid = "12345678900";
        String invalid = "!£$JAHFOA";
        Validator instance = new Validator();
        Boolean expResult = true;
        Boolean result = instance.isValidNHSID(valid);
        assertEquals(expResult, result);
        
        expResult = false;
        result = instance.isValidNHSID(invalid);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of isValidHCPID method, of class Validator.
     */
    @Test
    public void testIsValidHCPID() {
        System.out.println("isValidHCPID");
        String valid = "HCP5678";
        String invalid = "A!*$8-=";
        Validator instance = new Validator();
        Boolean expResult = true;
        Boolean result = instance.isValidHCPID(valid);
        assertEquals(expResult, result);
        
        expResult = false;
        result = instance.isValidHCPID(invalid);
        assertEquals(expResult, result);
 
    }

    /**
     * Test of isValidFirst method, of class Validator.
     */
    @Test
    public void testIsValidFirst() {
        System.out.println("isValidFirst");
        String valid = "Yukta";
        String invalid = "!DK-0-456-";
        Validator instance = new Validator();
        Boolean expResult = true;
        Boolean result = instance.isValidFirst(valid);
        assertEquals(expResult, result);
        
        expResult = false;
        result = instance.isValidFirst(invalid);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of isValidLast method, of class Validator.
     */
    @Test
    public void testIsValidLast() {
        System.out.println("isValidLast");
        String valid = "Shrestha";
        String invalid = "!293-4=*&";
        Validator instance = new Validator();
        Boolean expResult = true;
        Boolean result = instance.isValidLast(valid);
        assertEquals(expResult, result);
        
        expResult = false;
        result = instance.isValidLast(invalid);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of isValidDOB method, of class Validator.
     */
    @Test
    public void testIsValidDOB() {
        System.out.println("isValidDOB");
        LocalDate validDate = LocalDate.of(1996,5,9);
        LocalDate invalidDate = LocalDate.now();
        Validator instance = new Validator();
        assertTrue(instance.isValidDOB(validDate));
        assertFalse(instance.isValidDOB(invalidDate));
    }

    /**
     * Test of isValidAddress1 method, of class Validator.
     */
    @Test
    public void testIsValidAddress1() {
        System.out.println("isValidAddress1");
        String valid = "11A Pine Lane";
        String invalid = "$!2 Cow Lane";
        Validator instance = new Validator();
        Boolean expResult = true;
        Boolean result = instance.isValidAddress1(valid);
        assertEquals(expResult, result);
        
        expResult = false;
        result = instance.isValidAddress1(invalid);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of isValidAddress2 method, of class Validator.
     */
    @Test
    public void testIsValidAddress2() {
        System.out.println("isValidAddress2");
        String valid = "Kingston-Upon-Thames";
        String invalid = "12!^^*($";
        Validator instance = new Validator();
        Boolean expResult = true;
        Boolean result = instance.isValidAddress2(valid);
        assertEquals(expResult, result);
        
        expResult = false;
        result = instance.isValidAddress2(invalid);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of isValidPostcode method, of class Validator.
     */
    @Test
    public void testIsValidPostcode() {
        System.out.println("isValidPostcode");
        String valid = "KT5 8DF";
        String invalid = "not a postcode";
        Validator instance = new Validator();
        Boolean expResult = true;
        Boolean result = instance.isValidPostcode(valid);
        assertEquals(expResult, result);
        expResult = false;
        result = instance.isValidPostcode(invalid);
        assertEquals(expResult, result);
    }

    /**
     * Test of isValidTelephone method, of class Validator.
     */
    @Test
    public void testIsValidTelephone() {
        System.out.println("isValidTelephone");
        String valid = "07356828382";
        String invalid = "028%73AN";
        Validator instance = new Validator();
        Boolean expResult = true;
        Boolean result = instance.isValidTelephone(valid);
        assertEquals(expResult, result);
        
        expResult = false;
        result = instance.isValidTelephone(invalid);
        assertEquals(expResult, result);
    }
    
}
