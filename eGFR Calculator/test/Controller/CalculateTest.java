/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.CSVPatient;
import Model.PatientFactory;
import java.util.List;
import java.util.Map;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author k1604642
 */
public class CalculateTest {
    
    public CalculateTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of calculateForPatient method, of class Calculate.
     */
    
    @Test
    public void testCalculateForPatient() {
        System.out.println("calculateForPatient");
        CSVPatient patient = null;
        PatientFactory pf = new PatientFactory();
        patient = pf.createPatientFromCSV("21101997300", "Female", "Other", 21, 110.0);
        Double expResult = 58.0;
        Double result = Calculate.calculateForPatient(patient);
        assertEquals(expResult, result);
    }   
}
