/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Patient;
import Model.PatientFactory;
import static java.lang.Thread.sleep;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author k1423138
 */
public class DataAccessTest {

    public DataAccessTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {

    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {

    }

    /**
     * Test of openConnection method, of class DataAccess.
     */
    @Test
    public void testOpenConnection() {
        Boolean success = false;
        DataAccess instance = new DataAccess();
        try {
            instance.openConnection();
            success = instance.connected;

        } catch (SQLException ex) {
            Logger.getLogger(DataAccessTest.class.getName()).log(Level.SEVERE, null, ex);

        }
        try {
            instance.closeConnection();
        } catch (Exception ex) {
            Logger.getLogger(DataAccessTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        assertTrue(success);
    }

    /**
     * Test of closeConnection method, of class DataAccess.
     */
    @Test
    public void testCloseConnection() {
        System.out.println("closeConnection");
        boolean success = true;
        DataAccess instance = new DataAccess();
        try {
            instance.openConnection();
        } catch (SQLException ex) {
            Logger.getLogger(DataAccessTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            instance.closeConnection();
        } catch (Exception ex) {
            Logger.getLogger(DataAccessTest.class.getName()).log(Level.SEVERE, null, ex);
            success = false;
        }
        assertTrue(success);
    }

    /**
     * Test of getWorkingUser method, of class DataAccess.
     */
    @Test
    public void testGetWorkingUser() {
        System.out.println("getWorkingUser");
        String workingUserID = "12345678910";
        DataAccess instance = new DataAccess();
        Integer count = 0;
        try {
            instance.openConnection();
            ResultSet result = null;
            result = instance.getWorkingUser(workingUserID);
            while (result.next()) {
                count++;
            }
            instance.closeConnection();

        } catch (SQLException ex) {
            Logger.getLogger(DataAccessTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        assertTrue(count > 0);

    }

    /**
     * Test of updatePatient method, of class DataAccess.
     */
    @Test
    public void testUpdateMedicalInfo() {
        System.out.println("updatePatient");
        String patient = null;
        DataAccess instance = new DataAccess();
        String last = "";
        ResultSet rs = null;

        try {
            instance.openConnection();
            rs = instance.getWorkingUser("12345678910");
            while (rs.next()) {
                last = rs.getString("LastName");

                switch (last) {
                    case "Harris":
                        assertTrue(instance.updateMedicalInfo("12345678910", "Rafe", "Ford", "Male", "11 Town End House, High Street", "London", "KT1 1NA", "07494882536", "k1423138@kingston.ac.uk"));
                        break;
                    case "Ford":
                        assertTrue(instance.updateMedicalInfo("12345678910", "Rafe", "Harris", "Male", "11 Town End House, High Street", "London", "KT1 1NA", "07494882536", "k1423138@kingston.ac.uk"));
                        break;
                    default:
                        break;
                }
            }
            instance.closeConnection();
        } catch (SQLException ex) {
            Logger.getLogger(DataAccessTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Test of addPatientToDatabase method, of class DataAccess.
     */
    @Test
    public void testAddPatientToDatabase() {
        System.out.println("addPatientToDatabase");
        Boolean result = false;
        String nhsID = "45874236951";
        String password = "addPatient";
        String firstName = "Yukta";
        String lastName = "Shrestha";
        Date now = new Date(System.currentTimeMillis());
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        cal.add(Calendar.YEAR, -21);
        Date minusYears = new Date(cal.getTimeInMillis());
        LocalDate dob = minusYears.toLocalDate();
        String ethnicity = "Other";
        String gender = "Female";
        String address1 = "81 Burney Avenue";
        String address2 = "Kingston";
        String postcode = "KT5 8DF";
        String number = "07854236998";
        String email = "k1604642@kingston.ac.uk";
        String salt = "111";
        DataAccess instance = new DataAccess();
        try {
            instance.openConnection();

            result = instance.addPatientToDatabase(nhsID, password, firstName, lastName, dob, ethnicity, gender, address1, address2, postcode, number, email, salt);
            String sql = "DELETE FROM MedicalRecords WHERE NHSNumber = ?";
            String sql1 = "DELETE FROM Patient WHERE NHSNumber = ?";

            PreparedStatement ps = instance.conn.prepareStatement(sql);
            PreparedStatement ps1 = instance.conn.prepareStatement(sql1);
            ps.setString(1, "45874236951");
            ps1.setString(1, "45874236951");
            ps.executeUpdate();
            sleep(10000);
            ps1.executeUpdate();
            instance.closeConnection();

        } catch (SQLException ex) {
            Logger.getLogger(DataAccess.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(DataAccessTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        assertTrue(result);
    }

    /**
     * Test of loginPatientToDatabase method, of class DataAccess.
     */
    @Test
    public void testLoginPatientToDatabase() throws Exception {
        System.out.println("loginPatientToDatabase");
        String NHSNumber = "12345678910";
        String password = "testpass";
        DataAccess instance = new DataAccess();
        instance.openConnection();
        Boolean result = DataAccess.loginPatientToDatabase(NHSNumber, password);
        instance.closeConnection();
        assertTrue(result);
    }

    /**
     * Test of loginClinicianToDatabase method, of class DataAccess.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testLoginClinicianToDatabase() throws Exception {
        System.out.println("loginClinicianToDatabase");
        String hcpID = "HCP2001";
        String password = "8Q396b9D68";
        DataAccess instance = new DataAccess();
        instance.openConnection();
        Boolean result = DataAccess.loginClinicianToDatabase(hcpID, password);
        instance.closeConnection();
        assertTrue(result);

    }

    /**
     * Test of searchPatients method, of class DataAccess.
     */
    @Test
    public void testSearchPatients() {
        System.out.println("searchPatients");
        String nhsNumber = "12345678910";
        String lastName = "";
        String email = "k1423138@kingston.ac.uk";
        String postcode = "KT1 1NA";
        Integer count = 0;
        DataAccess instance = new DataAccess();
        try {
            instance.openConnection();
            ResultSet result = null;
            result = instance.searchPatients(nhsNumber, lastName, email, postcode);
            while (result.next()) {
                count++;
            }
            instance.closeConnection();
        } catch (SQLException e) {
        }
        assertTrue(count > 0);

    }

    /**
     * Test of getPreviousResults method, of class DataAccess.
     */
    @Test
    public void testGetPreviousResults() throws Exception {
        System.out.println("getPreviousResults");
        String NHSNumber = "12345678910";
        DataAccess instance = new DataAccess();
        Integer count = 0;
        try {
            instance.openConnection();
            ResultSet result = null;
            result = instance.getPreviousResults(NHSNumber);
            while (result.next()) {
                count++;
            }
            instance.closeConnection();
        } catch (SQLException e) {
        }
        assertTrue(count > 0);
    }

    /**
     * Test of addPatientResultToDatabase method, of class DataAccess.
     */
    @Test
    public void testAddPatientResultToDatabase() {
        System.out.println("addPatientResultToDatabase");
        String NHSNumber = "12345678910";
        String result_2 = "86";
        long time = System.currentTimeMillis();
        Date date = new java.sql.Date(time);

        DataAccess instance = new DataAccess();
        try {
            instance.openConnection();
            Boolean result = instance.addPatientResultToDatabase(NHSNumber, result_2, date);
            instance.closeConnection();
            assertTrue(result);
        } catch (SQLException e) {
            Logger.getLogger(DataAccess.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    /**
     * Test of isRegistered method, of class DataAccess.
     */
    @Test
    public void testIsRegistered() {
        System.out.println("isRegistered");
        String NHSNumber = "12345678910";
        DataAccess instance = new DataAccess();
        Boolean expResult = true;
        Boolean result = false;
        try {
            instance.openConnection();
            result = instance.isRegistered(NHSNumber);
            instance.closeConnection();
        } catch (SQLException ex) {
            Logger.getLogger(DataAccessTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        assertEquals(expResult, result);

    }

}
