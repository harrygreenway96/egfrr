/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Controller.DataAccess;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author k1423138
 */
public class PatientFactoryTest {
    
    public PatientFactoryTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of createPatientFromResultSet method, of class PatientFactory.
     */
    @Test
    public void testCreatePatientFromResultSet() {
        DataAccess da = new DataAccess();
        String nhsNumber = "12345678910";
        PatientFactory instance = new PatientFactory();
        try {
            da.openConnection();
              ResultSet rs = da.getWorkingUser(nhsNumber);
        Patient test = null;
        Patient test1 = null;
        test1 = instance.createPatientFromResultSet(rs);
        assertNotEquals(test, test1);
    da.closeConnection();
        } catch (SQLException ex) {
            Logger.getLogger(PatientFactoryTest.class.getName()).log(Level.SEVERE, null, ex);
        }
      
    }
}
