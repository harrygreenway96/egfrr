
package Model;
import Model.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author k1423138
 */
public class PatientContainer {
    private List<CSVPatient> patients = new ArrayList<>();
    //deletepatient
    public List<CSVPatient> getPatients() {
        return patients;
    }

    public void setPatients(List<CSVPatient> patients) {
        this.patients = patients;
    }
    
    public void addPatient(CSVPatient patient){
        this.patients.add(patient);
    }
    
    public void addPatients(List<CSVPatient> patients){
        this.patients.addAll(patients);
    }
}
