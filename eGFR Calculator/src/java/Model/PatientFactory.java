/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author k1423138
 */
public class PatientFactory {
    public CSVPatient createPatientFromCSV(String nhs, String sex, String ethnicity, Integer age, Double creatinine){
        CSVPatient userToCreate = null;
        userToCreate = new CSVPatient(nhs,sex,ethnicity,age,creatinine);
        return userToCreate;
    }
    public Patient createPatientFromResultSet(ResultSet patientRS){
        Patient userToCreate = null;
        try {
            while(patientRS.next()){
                try {
                    
                    String nhsNumber = patientRS.getString("NHSNumber");
                    String firstName = patientRS.getString("FirstName");
                    String lastName = patientRS.getString("LastName");
                    String address1 = patientRS.getString("Address1");
                    String address2 = patientRS.getString("Address2");
                    String postcode = patientRS.getString("Postcode");
                    String phoneNumber = patientRS.getString("PhoneNumber");
                    Date dateOfBirth = new java.util.Date(patientRS.getDate("DOB").getTime());
                    Calendar cal = Calendar.getInstance();
                    Date now = cal.getTime();
                    long milliDiff = Math.abs(now.getTime() - dateOfBirth.getTime());
                    long daysDiff = TimeUnit.DAYS.convert(milliDiff, TimeUnit.MILLISECONDS);
                    Integer years = (int) (daysDiff / 365);
                    String sex = patientRS.getString("Sex");
                    String ethnicity = patientRS.getString("Ethnicity");
                    String email = patientRS.getString("Email");
                    System.out.println(nhsNumber+firstName+lastName+address1+address2+postcode+phoneNumber+Integer.toString(years)+sex+ethnicity+email);
                    userToCreate = new Patient(nhsNumber,firstName,lastName,address1,address2,postcode,phoneNumber,years,sex,ethnicity,email);
                } catch (SQLException ex) {
                    Logger.getLogger(PatientFactory.class.getName()).log(Level.SEVERE, null, ex);
                }}
        } catch (SQLException ex) {
            Logger.getLogger(PatientFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        return userToCreate; 
    }
}