/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;


import java.time.Period;
import java.util.Date;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;

/**
 *
 * @author k1423138
 */
public class Patient {

    private String nhsNumber;
    private String firstName;
    private String lastName;
    private String address1;
    private String address2;
    private String postCode;
    private String phoneNumber;
    private Integer age;
    private String sex;
    private String ethnicity;
    private String email;
    private Double creatinineValue;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    

    public Patient() {
    }

    public Patient(String nhsNumber) {
        this.setNhsNumber(nhsNumber);
    }

    public Patient(String nhsNumber, String firstName, String lastName,
            String address1, String address2, String postCode, String phoneNumber, Integer age,
            String sex, String ethnicity, String email) {
        this.setNhsNumber(nhsNumber);
        this.setFirstName(firstName);
        this.setLastName(lastName);
        this.setAddress1(address1);
        this.setAddress2(address2);
        this.setPostCode(postCode);
        this.setPhoneNumber(phoneNumber);
        this.setAge(age);
        this.setSex(sex);
        this.setEthnicity(ethnicity);
        this.setEmail(email);

    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress1() {
        return address1;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Boolean isFemale() {
        if (getSex().equalsIgnoreCase("female")) {
            return true;
        } else {
            return false;
        }
    }

    public Boolean isBlack() {
        if (getEthnicity().equalsIgnoreCase("black")) {
            return true;
        } else {
            return false;
        }
    }

    public String getNhsNumber() {
        return nhsNumber;
    }

    public void setNhsNumber(String nhsNumber) {
        this.nhsNumber = nhsNumber;
    }



    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getEthnicity() {
        return ethnicity;
    }

    public void setEthnicity(String ethnicity) {
        this.ethnicity = ethnicity;
    }

    public Double getCreatinineValue() {
        return creatinineValue;
    }

    public void setCreatinineValue(Double creatinineValue) {
        this.creatinineValue = creatinineValue;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;

    }

}