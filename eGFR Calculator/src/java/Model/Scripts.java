/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author k1423138
 */
public class Scripts {
    public String getScriptsForGuestCalc(){
        String rtnString = "<script type=\"text/javascript\">\n" +
"            function getStage(egfr){\n" +
"                if (egfr >= 90){\n" +
"                    return \"1\";\n" +
"                }else if(60 <= egfr && egfr <= 89){\n" +
"                    return \"2\";\n" +
"                }else if (45 <= egfr && egfr <= 59){\n" +
"                    return \"3A\"\n" +
"                }else if (30 <= egfr && egfr <= 44){\n" +
"                    return \"3B\";\n" +
"                }else if (15 <= egfr && egfr <= 29){\n" +
"                    return \"4\";\n" +
"                }else if (egfr < 15){\n" +
"                    return \"5\";\n" +
"                }\n" +
"            }\n" +
"            \n" +
"            function Calculate(creat, age, female, black, convert) {\n" +
"                if (convert) {\n" +
"                    creat = creat / 88.4;\n" +
"                }\n" +
"                calcA = Math.pow((creat), -1.154);\n" +
"                calcB = Math.pow(age, -0.203);\n" +
"                calcC = 1.00;\n" +
"                if (female)\n" +
"                    calcC = 0.742;\n" +
"                calcD = 1.00;\n" +
"                if (black)\n" +
"                    calcD = 1.210;\n" +
"                return Math.trunc(186 * calcA * calcB * calcC * calcD);\n" +
"            }\n" +
"            $(document).ready(function(){\n" +
"                $(\"#result\").hide();\n" +
"                $(\"#calculate\").click(function(){\n" +
"                    document.getElementById(\"r1\").style = \"background-color: #f2f2f2;\";\n" +
"                    document.getElementById(\"r2\").style = \"background-color: #ddd;\";\n" +
"                    document.getElementById(\"r3A\").style = \"background-color: #f2f2f2;\";\n" +
"                    document.getElementById(\"r3B\").style = \"background-color: #f2f2f2;\";\n" +
"                    document.getElementById(\"r4\").style = \"background-color: #ddd;\";\n" +
"                    document.getElementById(\"r5\").style = \"background-color: #f2f2f2;\";\n" +
"                    female = document.getElementById(\"gender\").value === \"Female\" ? true : false;\n" +
"                    black = document.getElementById(\"ethnicity\").value === \"Black\" ? true : false;\n" +
"                    age = document.getElementById(\"age\").value;\n" +
"                    creat = $(\"#creatinine\").val();\n" +
"                    convert = document.getElementById(\"unit\").value === \"1\" ? true : false;\n" +
"                    result = Calculate(creat, age, female, black, convert);\n" +
"                    stage = getStage(result);\n" +
"                    switch(stage){\n" +
"                        case \"1\":\n" +
"                            document.getElementById(\"r1\").style = \"background-color: #00f238;\";\n" +
"                            break;\n" +
"                        case \"2\":\n" +
"                            document.getElementById(\"r2\").style = \"background-color: #70f100;\";\n" +
"                            break;\n" +
"                        case \"3A\":\n" +
"                            document.getElementById(\"r3A\").style = \"background-color: #cfef00;\";\n" +
"                            document.getElementById(\"r3B\").style = \"background-color: #cfef00;\";\n" +
"                            break;\n" +
"                        case \"3B\":\n" +
"                            document.getElementById(\"r3A\").style = \"background-color: #efc700;\";\n" +
"                            document.getElementById(\"r3B\").style = \"background-color: #efc700;\";\n" +
"                            break;\n" +
"                        case \"4\":\n" +
"                            document.getElementById(\"r4\").style = \"background-color: #efa300;\";\n" +
"                            break;\n" +
"                        case \"5\":\n" +
"                            document.getElementById(\"r5\").style = \"background-color: #f06400;\";\n" +
"                            break;\n" +
"                    }\n" +
"                    document.getElementById('resultLabel').innerHTML = result;\n" +
"                    $(\"#result\").show();\n" +
"                    $(\"#calc\").hide();\n" +
"                });\n" +
"            });\n" +
"        </script>";
        return rtnString;
    }
    public String getScriptsForPatientCalc(String age,String female,String black){
        String rtnString = "<script> \n"
                + "function Calculate(creat, age, female, black, convert) {\n" +
"                if (convert) {\n" +
"                    creat = creat / 88.4;\n" +
"                }\n" +
"                calcA = Math.pow((creat), -1.154);\n" +
"                calcB = Math.pow(age, -0.203);\n" +
"                calcC = 1.00;\n" +
"                if (female)\n" +
"                    calcC = 0.742;\n" +
"                calcD = 1.00;\n" +
"                if (black)\n" +
"                    calcD = 1.210;\n" +
"                return Math.trunc(186 * calcA * calcB * calcC * calcD);\n" +
"            }\n" +
"            function getStage(egfr){\n" +
"                if (egfr >= 90){\n" +
"                    return \"1\";\n" +
"                }else if(60 <= egfr && egfr <= 89){\n" +
"                    return \"2\";\n" +
"                }else if (45 <= egfr && egfr <= 59){\n" +
"                    return \"3A\"\n" +
"                }else if (30 <= egfr && egfr <= 44){\n" +
"                    return \"3B\";\n" +
"                }else if (15 <= egfr && egfr <= 29){\n" +
"                    return \"4\";\n" +
"                }else if (egfr < 15){\n" +
"                    return \"5\";\n" +
"                }\n" +
"            }\n" +
"            \n" +
"            \n" +
"            $(document).ready(function () {\n" +
"                $(\"#result\").hide();\n" +
"                $(\"#calculate\").click(function () {\n" +
"                    document.getElementById(\"r1\").style = \"background-color: #f2f2f2;\";\n" +
"                    document.getElementById(\"r2\").style = \"background-color: #ddd;\";\n" +
"                    document.getElementById(\"r3A\").style = \"background-color: #f2f2f2;\";\n" +
"                    document.getElementById(\"r3B\").style = \"background-color: #f2f2f2;\";\n" +
"                    document.getElementById(\"r4\").style = \"background-color: #ddd;\";\n" +
"                    document.getElementById(\"r5\").style = \"background-color: #f2f2f2;\";\n" +
"\n" +
"                    $(\"#calc\").hide();\n" +
"                    creat = document.getElementById('CreatinineValue').value;\n" +
"                    convert = false;\n" +
"                    option = $(\"#unit\").val();\n" +
"                    if (option === \"1\") {\n" +
"                        convert = true;\n" +
"                    }\n" +
"                    result = Calculate(creat,"+age+","+female+","+black+",convert);\n" +
"                    stage = getStage(result);\n" +
"                    switch(stage){\n" +
"                        case \"1\":\n" +
"                            document.getElementById(\"r1\").style = \"background-color: #00f238;\";\n" +
"                            break;\n" +
"                        case \"2\":\n" +
"                            document.getElementById(\"r2\").style = \"background-color: #70f100;\";\n" +
"                            break;\n" +
"                        case \"3A\":\n" +
"                            document.getElementById(\"r3A\").style = \"background-color: #cfef00;\";\n" +
"                            document.getElementById(\"r3B\").style = \"background-color: #cfef00;\";\n" +
"                            break;\n" +
"                        case \"3B\":\n" +
"                            document.getElementById(\"r3A\").style = \"background-color: #efc700;\";\n" +
"                            document.getElementById(\"r3B\").style = \"background-color: #efc700;\";\n" +
"                            break;\n" +
"                        case \"4\":\n" +
"                            document.getElementById(\"r4\").style = \"background-color: #efa300;\";\n" +
"                            break;\n" +
"                        case \"5\":\n" +
"                            document.getElementById(\"r5\").style = \"background-color: #f06400;\";\n" +
"                            break;\n" +
"                    }\n" +
"                    document.getElementById('resultLabel').innerHTML = result;\n" +
"                    $(\"#result\").show();\n" + 
               
                
"                });\n" +
"            }" +
              
                 ");"
                + "</script>";
        return rtnString;
    }
    public String getScriptsForClinicianPage(){
        String rtnString = "<script type=\"text/javascript\">\n" +
"    $(document).ready(function(){\n" +
"        $(\"#newPatient\").hide();\n" +
"        $(\"#showAddNew\").click(function(){\n" +
"            $(\"#search\").hide();\n" +
"            $(\"#newPatient\").show();\n" +
"        });\n" +
"        $(\"#showSearch\").click(function(){\n" +
"            $(\"#newPatient\").hide();\n" +
"            $(\"#search\").show();\n" +
"        });\n" +
"    });\n" +
"</script>";
        return rtnString;
    }
    public String getResult() {
        String rtnString ="function Value1()\n" +
"{\n" +
"    document.getElementById(\"X\").value = document.getElementById(\"resultLabel\").innerHTML;\n" +
"    var value2 = document.getElementById(\"resultLabel\").innerHTML;\n" +
"    var hidden = document.getElementById(\"hidden\").value;\n" +
"    hidden = value2;\n" +
"}\n";

        return rtnString;
    }
    public String getScriptsForUploadPatientsPage(){
        String rtn = "<script type=\"text/javascript\">"
                    + "function showHide(div) {\n" +
                    "  var x = document.getElementById(div);\n" +
                    "  if (x.style.display === \"none\") {\n" +
                    "    x.style.display = \"block\";\n" +
                    "  } else {\n" +
                    "    x.style.display = \"none\";\n" +
                    "  }\n" +
                    "}"
                    + "$(document).ready(function (){"
                        + "$(\"#exclusions\").hide();"
                        + "$(\"#results\").hide();"
                        + "$(\"#accounts\").hide();"
                        + "$(\"#stored\").hide();"
                        + "$(\"#exclusionsButton\").click(function(){"
                            + " showHide(\"exclusions\");"
                        + "});"
                        + "$(\"#resultsButton\").click(function(){"
                            + " showHide(\"results\");"
                        + "});"
                        + "$(\"#accountsButton\").click(function(){"
                            + " showHide(\"accounts\");"
                        + "});"
                        + "$(\"#storedButton\").click(function(){"
                            + " showHide(\"stored\");"
                        + "});"
                    + "});"
                    + "</script>";
        return rtn;
    }
}

