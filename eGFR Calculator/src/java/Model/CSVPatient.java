/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author k1423138
 */
public class CSVPatient {
    private String nhsNumber;
    private String sex;
    private String ethnicity;
    private Integer Age;
    private Double creatinine;

    public CSVPatient(String nhsNumber, String sex, String ethnicity, Integer Age, Double creatinine) {
        this.nhsNumber = nhsNumber;
        this.sex = sex;
        this.ethnicity = ethnicity;
        this.Age = Age;
        this.creatinine = creatinine;
    }
    

    public Boolean isBlack(){
        return (this.getEthnicity().equals("B"));
    }
    public Boolean isFemale(){
        return (this.getSex().equals("1"));
    }
    public String getNhsNumber() {
        return nhsNumber;
    }

    public void setNhsNumber(String nhsNumber) {
        this.nhsNumber = nhsNumber;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getEthnicity() {
        return ethnicity;
    }

    public void setEthnicity(String ethnicity) {
        this.ethnicity = ethnicity;
    }

    public Integer getAge() {
        return Age;
    }

    public void setAge(Integer Age) {
        this.Age = Age;
    }

    public Double getCreatinine() {
        return creatinine;
    }

    public void setCreatinine(Double creatinine) {
        this.creatinine = creatinine;
    }
    
}
