/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.time.LocalDate;
import java.sql.Date;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author k1423138
 */
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validator {


    public Boolean isValidNHSID(String field) {
        if (field.isEmpty() || (field.length() < 11)) {
            return false;
        }
        try {
            Double nhsID = Double.parseDouble(field);

        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    public Boolean isValidHCPID(String field) {
        Boolean valid = true;
        String hcp = field.substring(0,3);
        String four = field.substring(3);
        if (!hcp.equalsIgnoreCase("HCP")){
            valid = false;
        }
        try{
            Integer i = Integer.parseInt(four);
        }catch(NumberFormatException nfe){
            valid = false;
        }
        
        return valid;
  
    }
    

    public Boolean isValidFirst(String field) {

        Pattern digit = Pattern.compile("[0-9]");
        Pattern special = Pattern.compile("[!@#$%*()_+=|<>?{}\\[\\]~]|`¬");
        Matcher a = digit.matcher(field);
        Matcher b = special.matcher(field);
        boolean c = a.find();
        boolean d = b.find();

        return !(c || d);
    }

    public Boolean isValidLast(String field) {

        Pattern special = Pattern.compile("[!@#$%*()_+=|<>?{}\\[\\]~]|`¬");
        Matcher a = special.matcher(field);
        return !(a.find());
    }

    public Boolean isValidDOB(LocalDate field) {
        Date dob = Date.valueOf(field);
        java.util.Date dateOfBirth = new java.util.Date(dob.getTime());
        Calendar cal = Calendar.getInstance();
        java.util.Date now = cal.getTime();
        long milliDiff = Math.abs(now.getTime() - dateOfBirth.getTime());
        long daysDiff = TimeUnit.DAYS.convert(milliDiff, TimeUnit.MILLISECONDS);
        Integer years = (int) (daysDiff / 365);
        return (years >= 18);
    }

    public Boolean isValidAddress1(String field) {
        Pattern special = Pattern.compile("[!@#$%*()_+=|<>?{}\\[\\]~`¬]");
        Matcher a = special.matcher(field);        
        return !(a.find());
    }

    public Boolean isValidAddress2(String field) {
        Pattern special = Pattern.compile("[!@#$%*()_+=|<>?{}\\[\\]~`¬]");
        Matcher a = special.matcher(field);        
        return !(a.find());
    }

    public Boolean isValidPostcode(String field) {
        String regex = "^[A-Z]{1,2}[0-9R][0-9A-Z]? [0-9][ABD-HJLNP-UW-Z]{2}$";
        field = field.toUpperCase();
        Pattern postcode = Pattern.compile(regex);
        Matcher a = postcode.matcher(field);
        return (a.find());
    }

    public Boolean isValidTelephone(String field) {
        Boolean valid = true;
        if (field.isEmpty() || (field.length() < 11)) {
            valid = false;
        }
        try {
            Double telephone = Double.parseDouble(field);

        } catch (NumberFormatException nfe) {
            valid = false;
        }
        return valid;
    }

}
