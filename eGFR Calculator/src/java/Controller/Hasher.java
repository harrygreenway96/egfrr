package Controller;
import java.math.BigInteger; 
import java.security.MessageDigest; 
import java.security.NoSuchAlgorithmException; 

/**
 *
 * @author k1423138
 */
public class Hasher {
    public String getSHA(String hashThis)  //Borrowed from my FYP   TODO: make appropriate changes so I don't flag up for plagirising my own work
    { 
        try { 
            MessageDigest md = MessageDigest.getInstance("SHA-256"); //Get Hashing Algorithm
            byte[] digest = md.digest(hashThis.getBytes()); //Convert string to byte value using hashing algorithm
            BigInteger byteVal = new BigInteger(1, digest); //Convert byte value hash to hex
            String hash = byteVal.toString(16);             //convert hash to string
  
            while (hash.length() < 64) {                    //Left Pad with zeros if length < 64
                hash = "0" + hash; 
            }
            return hash.toUpperCase(); 
        } 
        catch (NoSuchAlgorithmException e) { 
            System.out.println("Incorrect algorithm: " + e); 
            return null; 
        }
    }
}    
   