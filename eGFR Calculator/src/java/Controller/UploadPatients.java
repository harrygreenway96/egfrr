/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.CSVPatient;
import Model.PatientContainer;
import Model.PatientFactory;
import Model.Validator;
import Controller.Calculate;
import Model.Scripts;
import Model.Styles;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import static java.lang.System.currentTimeMillis;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

/**
 *
 * @author k1423138
 */
@MultipartConfig
public class UploadPatients extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            Boolean added = false;
            PatientContainer pc = new PatientContainer();
            DataAccess da = new DataAccess();
            PatientFactory patientFactory = new PatientFactory();
            List<String> exclusions = new ArrayList<>();
            List<Double> results = new ArrayList<>();
            List<String> notAdded = new ArrayList<>();
            List<CSVPatient> resultAddedList = new ArrayList<>();
            List<CSVPatient> accountCreatedList = new ArrayList<>();
            Part csvPart = request.getPart("file");
            InputStream csvContent = csvPart.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(csvContent));
            CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT);
            List<CSVRecord> csvList = csvParser.getRecords();
            csvList.remove(0);
            Iterator<CSVRecord> i = csvList.iterator();
            while (i.hasNext()) {
                CSVRecord cur = i.next();
                String nhs = cur.get(0);
                String sex = cur.get(1);
                String ethnicity = cur.get(2);
                Integer age = null;
                Double creat = null;
                try {
                    age = Integer.parseInt(cur.get(3));
                    creat = Double.parseDouble(cur.get(4));
                } catch (NumberFormatException n) {
                    exclusions.add(nhs);
                } finally {
                    if ((creat != null) && (creat > 0) && (creat <= 200)) {
                        CSVPatient csvp = patientFactory.createPatientFromCSV(nhs, sex, ethnicity, age, creat);
                        pc.addPatient(csvp);
                    } else {
                        exclusions.add(nhs);
                    }
                }
            }
            Date now = new Date(currentTimeMillis());
            pc.getPatients().stream().forEach(p -> results.add(Calculate.calculateForPatient(p)));
            try {
                da.openConnection();
                for (CSVPatient p : pc.getPatients()) {
                    Integer counter = 0;
                    if (da.isRegistered(p.getNhsNumber())){
                        added = da.addPatientResultToDatabase(p.getNhsNumber(),results.get(counter).toString(), now);
                    }else{
                        if(da.addCSVPatientToDatabase(p)){
                            accountCreatedList.add(p);
                            added = da.addPatientResultToDatabase(p.getNhsNumber(),results.get(counter).toString(), now);
                        }
                    }
                    if (!added){
                        notAdded.add(p.getNhsNumber());
                    }else{
                        resultAddedList.add(p);
                    }
                    counter++;
                }
                da.closeConnection();
            } catch (SQLException ex) {
                Logger.getLogger(UploadPatients.class.getName()).log(Level.SEVERE, null, ex);
            }
                     
            Styles style = new Styles();
            Scripts script = new Scripts();
            out.print("<!DOCTYPE HTML>");
            out.print("<html>");
            out.print("<head>");
            out.print("<meta charset=\"UTF-8\">");
            out.print(style.getStyleForNavBar());
            out.print(style.getStyleForResultTable());
            out.print("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">");
            out.print("<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js\"></script>");
            out.print(script.getScriptsForUploadPatientsPage());
            request.getRequestDispatcher("/WEB-INF/navbar.jsp").include(request,response);
            out.print("There were " + Integer.toString(exclusions.size()) + " exclusions made due to incorrect format or unexpected values:<input type=\"button\" id=\"exclusionsButton\" value=\"Show/hide\" /></br>");
            out.print("<div id=\"exclusions\"");
            exclusions.stream().forEach(e -> out.print(e + "</br></br>"));
            out.print("</div>");
            out.print("Results (format NHS Number: eGFR/μmol):<input type=\"button\" id=\"resultsButton\" value=\"Show/hide\" /></br>");
            out.print("<div id=\"results\">");
            for(Integer x = 0;x<pc.getPatients().size();x++){
                out.print(pc.getPatients().get(x).getNhsNumber()+" : "+results.get(x).toString()+"</br>");
            }
            out.print("</div>");
            out.print(Integer.toString(accountCreatedList.size())+" Accounts created for the following users (Default password is NHS Number):<input type=\"button\" id=\"accountsButton\" value=\"Show/hide\" /></br>");
            out.print("<div id=\"accounts\">");
            accountCreatedList.stream().forEach(p->out.print(p.getNhsNumber()+"</br>"));
            out.print("</div>");
            out.print(Integer.toString(resultAddedList.size())+" Results successfully stored for the following users:<input type=\"button\" id=\"storedButton\" value=\"Show/hide\" /></br>");
            out.print("<div id=\"stored\">");
            resultAddedList.stream().forEach(p->out.print(p.getNhsNumber()+"</br>"));
            out.print("</div>");
            
            

        }
    }

        // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
        /**
         * Handles the HTTP <code>GET</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doGet
        (HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            processRequest(request, response);
        }

        /**
         * Handles the HTTP <code>POST</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doPost
        (HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            processRequest(request, response);
        }

        /**
         * Returns a short description of the servlet.
         *
         * @return a String containing servlet description
         */
        @Override
        public String getServletInfo
        
            () {
        return "Short description";
        }// </editor-fold>

    }
