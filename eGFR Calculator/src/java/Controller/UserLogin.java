/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;
import Controller.*;
import Model.Styles;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Cookie;  
import javax.servlet.http.HttpSession;
/**
 *
 * @author k1527629
 */
public class UserLogin extends HttpServlet {
        Boolean loginSuccesful = false;
        Cookie loginCookie = null;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws java.sql.SQLException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
         Styles style = new Styles();
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.print(style.getStyleForNavBar());
            request.getRequestDispatcher("WEB-INF/navbar.jsp").include(request, response);
            
            DataAccess dataAccess = new DataAccess();
            dataAccess.openConnection();
            String NHSNumber = request.getParameter("userID");
            String Password = request.getParameter("password");
            String radioResult = request.getParameter("typeRadio");
            System.out.println(NHSNumber+" "+Password+" "+radioResult);
            
            String HCPid = NHSNumber;
            
            try {
                if (radioResult.equals("Patient") && DataAccess.loginPatientToDatabase(NHSNumber, Password)){
//                    loginCookie = null;
//                    loginSuccesful = true;
//                    loginCookie = new Cookie("loginCookie",NHSNumber);
//                    loginCookie.setMaxAge(-1);
//                    System.out.println("Great success");
//                    response.sendRedirect("eGFR_Calculator/");
                    HttpSession session = request.getSession();
                    session.setAttribute("ID", NHSNumber);
                    session.setAttribute("Type", "Patient");
                    session.setAttribute("WorkingUser", NHSNumber);
                    
                    
                    response.sendRedirect(request.getContextPath() +"/");
                    
                }
                else if (radioResult.equals("Clinician") && DataAccess.loginClinicianToDatabase(HCPid, Password)){
                    HttpSession session = request.getSession();
                    session.setAttribute("ID", HCPid);
                    session.setAttribute("Type", "Clinician");
                    session.setAttribute("WorkingUser","");
                    
                    response.sendRedirect(request.getContextPath() +"/");
                }    
                    
                    else {
                    HttpSession session = request.getSession();
                    session.setAttribute("ID", null);
                    session.setAttribute("Type", null);
                    session.setAttribute("WorkingUser", null);
                    session.invalidate();
                    session = null;


                    request.getRequestDispatcher("loginpage.jsp").include(request, response);
                    
                }
            
            } catch (SQLException ex) {
                Logger.getLogger(UserLogin.class.getName()).log(Level.SEVERE, null, ex);
            }
               dataAccess.closeConnection();
        }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            try {
                processRequest(request, response);
            } catch (SQLException ex) {
                Logger.getLogger(UserLogin.class.getName()).log(Level.SEVERE, null, ex);
            }
    }
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            try {
                processRequest(request, response);
            } catch (SQLException ex) {
                Logger.getLogger(UserLogin.class.getName()).log(Level.SEVERE, null, ex);
            }
 
    }
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}