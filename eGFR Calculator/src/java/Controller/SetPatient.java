/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Styles;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author k1423138
 */
public class SetPatient extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            HttpSession session = request.getSession();
            Styles style = new Styles();
            if (((String)session.getAttribute("Type")).equals("Clinician")){//check user is a clin before showing them this page, else redirect to index
                String id = request.getParameter("id");
                session.setAttribute("WorkingUser", id);
                DataAccess da = new DataAccess();
                ResultSet patRS = null;
                
                da.openConnection();
                patRS = da.getWorkingUser(id);
                String first = null;
                String last = null;
                try {
                    while(patRS.next()){
                        first = patRS.getString("FirstName");
                        last = patRS.getString("LastName");
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(SetPatient.class.getName()).log(Level.SEVERE, null, ex);
                }
                da.closeConnection();
                
                out.print("<!DOCTYPE HTML>");
                out.print("<html>");
                out.print("<head>");
                out.print("<meta charset=\"UTF-8\">");
                out.print("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">");
                out.print("<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js\"></script>");
                out.print(style.getStyleForNavBar());
                out.print(style.getStyleForResultTable());

              
                request.getRequestDispatcher("/WEB-INF/navbar.jsp").include(request, response);
                out.print("</br>");
                out.print("Performing Calculation for "+first+" "+last+", NHS Number: "+id);
                request.getRequestDispatcher("WEB-INF/PatientCalc.jsp").include(request, response);
            }else{
                response.sendRedirect(request.getContextPath() +"/");
            }
        } catch (SQLException ex) {
            Logger.getLogger(SetPatient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
