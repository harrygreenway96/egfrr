/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Styles;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author k1423138
 */
public class Search extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Styles style = new Styles();
        HttpSession session = request.getSession(false);
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.print("<!DOCTYPE HTML>");
            out.print("<html>");
            out.print("<head>");
            out.print("<meta charset=\"UTF-8\">");
            out.print("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">");
            out.print("<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js\"></script>");
            out.print(style.getStyleForNavBar());
            out.print(style.getStyleForResultTable());

            out.print("</head>");
            request.getRequestDispatcher("/WEB-INF/navbar.jsp").include(request, response);

            DataAccess da = new DataAccess();
            String nhsNumber = request.getParameter("nhsID");
            String lastName = request.getParameter("last");
            String email = request.getParameter("email");
            String postcode = request.getParameter("postcode");
            if (nhsNumber.isEmpty() && lastName.isEmpty() && email.isEmpty() && postcode.isEmpty()) {
                response.sendRedirect(request.getContextPath() + "?empty=true");
            } else {
                da.openConnection();
                ResultSet patientRS = da.searchPatients(nhsNumber, lastName, email, postcode);
                int size = 0;
                if (patientRS != null) {
                    patientRS.last();
                    size = patientRS.getRow();
                    patientRS.beforeFirst();
                }
                switch (size) {
                    case 0:
                        response.sendRedirect(request.getContextPath() + "?found=false");
                        break;
                    case 1:
                        if (patientRS != null) {
                            while (patientRS.next()) {
                                String nhs = patientRS.getString("NHSNumber");
                                String first = patientRS.getString("FirstName");
                                String last = patientRS.getString("LastName");
                                session.setAttribute("WorkingUser", nhs);
                                out.print("</br>");
                                out.print("One patient with matching details found.</br>");
                                out.print("Performing Calculation for " + first + " " + last + ", NHS Number: " + nhs);
                                request.getRequestDispatcher("/WEB-INF/PatientCalc.jsp").include(request, response);
                            }
                        }
                        break;
                    default:
                        if (patientRS != null) {
                            out.print("<table id=\"searchResults\">");
                            out.print("<tr><th>NHS Number</th><th>First Name</th><th>Last Name</th><th>Postcode</th><th>Email</th></tr>");
                            while (patientRS.next()) {
                                String id = patientRS.getString("NHSNumber");
                                out.print(""
                                        + "<tr>"
                                        + "     <td><a href=\"SetPat?id=" + id + "\">" + patientRS.getString("NHSNumber") + "</td>"
                                        + "     <td>" + patientRS.getString("FirstName") + "</td>"
                                        + "     <td>" + patientRS.getString("LastName") + "</td>"
                                        + "     <td>" + patientRS.getString("Postcode") + "</td>"
                                        + "     <td>" + patientRS.getString("Email") + "</td>"
                                        + "</tr>");
                            }
                            out.print("</table>");
                        }
                }
                da.closeConnection();
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(Search.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
