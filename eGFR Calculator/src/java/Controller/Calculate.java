/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;
import Model.*;
import java.util.HashMap;
import java.util.Map;
import java.util.List;


/**
 *
 * @author k1423138
 */
public interface Calculate {
    //eGFR = 186 x (Creat / 88.4)^-1.154 x (Age)^-0.203 x (0.742 if female) x (1.210 if black)
                    //a                         b               c                   d
    public static Double calculateForPatient(CSVPatient patient){
        
        Double egfr;
        Double calcA = Math.pow((patient.getCreatinine()/88.4), -1.154);
        Double calcB = Math.pow(patient.getAge(), -0.203);
        Double calcC = 1.000;
        if (patient.isFemale()){
            calcC = 0.742;
        }
        Double calcD = 1.000;
        if (patient.isBlack()){
            calcD = 1.210;
        }
        egfr = 186 * calcA * calcB * calcC * calcD;
        return Math.floor(egfr);
    }
    public static Map<String,Double> calculateForPatients(List<CSVPatient> patients){ //Redundant code
        Map<String,Double> results = new HashMap<>();
        for (CSVPatient p : patients){
            Double egfr = calculateForPatient(p);
            String id = p.getNhsNumber();
            results.put(id, egfr);
        }
        return results;
    }
    
}
