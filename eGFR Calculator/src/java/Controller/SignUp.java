/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Styles;
import Model.Validator;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author k1423138
 */
public class SignUp extends HttpServlet { //URL pattern is /PatientSignUp

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    public int getSalt() {
        Random rand = new Random();
        int salt = rand.nextInt(999) + 1;
        return salt;
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
             Styles style = new Styles();
       try (PrintWriter out = response.getWriter()) { 
             Validator validator = new Validator();  
        Map<String,Boolean> validMap = new HashMap<>();
        response.setContentType("text/html;charset=UTF-8");
        //Get Parameters
        DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE;
        String nhsID = request.getParameter("NHSID");
        String password = request.getParameter("password");
        String firstName = request.getParameter("FirstName");
        String lastName = request.getParameter("LastName");
        LocalDate dob = LocalDate.parse(request.getParameter("date"), formatter);
        String ethnicity = request.getParameter("ethnicity");
        String gender = request.getParameter("gender");
        String address1 = request.getParameter("address1");
        String address2 = request.getParameter("address2");
        String postcode = request.getParameter("postcode");
        String number = request.getParameter("number");
        String email = request.getParameter("email");
        String notes = request.getParameter("notes");//For future
        validMap.put("nhsID", (!("".equals(nhsID))&&(validator.isValidNHSID(nhsID))));
        validMap.put("firstName", (!("".equals(firstName))&&(validator.isValidFirst(firstName))));
        validMap.put("lastName",(!("".equals(lastName))&&(validator.isValidLast(lastName))));
        validMap.put("dob",(validator.isValidDOB(dob)));
        validMap.put("address1", (!("".equals(address1))&&(validator.isValidAddress1(address1))));
        validMap.put("address2", (!("".equals(address2))&&(validator.isValidAddress2(address2))));
        validMap.put("postcode", (!("".equals(postcode))&&(validator.isValidPostcode(postcode))));
        validMap.put("number", (!("".equals(number))&&(validator.isValidTelephone(number))));
        Collection<Boolean> col = validMap.values();
        col.stream().forEach(b->out.print(b.toString()+"</br>"));
        if(validMap.get("nhsID")&&validMap.get("firstName")&&validMap.get("lastName")&&validMap.get("dob")&&validMap.get("address1")&&validMap.get("address2")&&validMap.get("postcode")&&validMap.get("number")){
            //Prepare password
            String salt = Integer.toString(getSalt());
            Hasher hasher = new Hasher();
            password = hasher.getSHA(password+salt);

                out.print(style.getStyleForNavBar());
                request.getRequestDispatcher("/WEB-INF/navbar.jsp").include(request, response);
                /* TODO output your page here. You may use following sample code. */
                DataAccess d = new DataAccess();
                d.openConnection();
                if (d.addPatientToDatabase(nhsID, password, firstName, lastName, dob, ethnicity, gender, address1, address2, postcode, number, email, salt)){
                out.println("account created");
                d.closeConnection();
                }else{out.println("error");}

            }else{
                StringBuilder sb = new StringBuilder();
                Integer count = 1;
                for (String s : validMap.keySet()){
                    if(count == 1){
                    sb.append("?");
                    }
                    if (!validMap.get(s)){
                        sb.append(s).append("=").append(!validMap.get(s)).append("&");
                    }
                    count++;
                    
                }
                
                response.sendRedirect(request.getContextPath()+"/signup.jsp"+sb.toString());
            }
        } catch (SQLException ex) {
            Logger.getLogger(SignUp.class.getName()).log(Level.SEVERE, null, ex);

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
