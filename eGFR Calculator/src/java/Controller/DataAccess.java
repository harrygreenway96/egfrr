
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.CSVPatient;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.sql.PreparedStatement;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Random;

/**
 *
 * @author k1423138 Server: sql2.freemysqlhosting.net Name: sql2276708 Username:
 * sql2276708 Password: cH9%wY1! Port number: 3306
 */
public class DataAccess {

    public static Connection conn = null;
    Boolean connected = false;
    Statement statement = null;
    ResultSet result = null;

    public void openConnection() throws SQLException { //Establish connection to database
        
        DataAccess dataAccess = new DataAccess();
        try {
            Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
            String dbName = "sql2281154";
            String serverName = "sql2.freemysqlhosting.net:3306";

            String url = "jdbc:mySQL://" + serverName + "/" + dbName;        //Access db here using:
            //rafe.harris@hotmail.co.uk : feU4y^&vhWoSVM1S
            conn = DriverManager.getConnection(url, "sql2281154", "kQ1%bC7!"); //Follow this link every week to ensure the db doesn't delete itself again:
            //https://www.freemysqlhosting.net/extension/?acc=eb044afeac08f76e
            connected = conn != null;
        } //Exceptions - handle any errors (look in console)
        catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
            System.out.println(ex);
        }
        System.out.println(connected);

    }

    public void closeConnection() {//Close connection to database
        try {
            conn.close();
            connected = false;
        } catch (Exception ex) {
            System.out.println("Exception: " + ex);
        }
    }

    public ResultSet queryDatabse(String query) { //Runs any query on database
        try {
            statement = conn.createStatement();
            result = statement.executeQuery(query);

        } catch (Exception ex) {
            System.out.println("Exception: " + ex);
        }
        return result;
    }

    public ResultSet getWorkingUser(String workingUserID) throws SQLException {
        String getUserSQL = "SELECT * FROM MedicalRecords WHERE NHSNumber = ?";
        PreparedStatement userPS;
        ResultSet userRS = null;
        try {
            userPS = conn.prepareStatement(getUserSQL);
            userPS.setString(1, workingUserID);
            userRS = userPS.executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(DataAccess.class.getName()).log(Level.SEVERE, null, ex);

        }
        return userRS;

    }

    public void tearDownQuery() throws SQLException {   //
        if (result != null) {
            try {
                result.close();
            } catch (Exception ex) {
                System.out.println("Exception: " + ex);
                conn.close();
            }
        }
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException ex) {
                System.out.println("Exception: " + ex);
                conn.close();
            }
        }
    }

    public Boolean updateMedicalInfo(String nhsID, String first, String last, String gender, String address1,
            String address2, String postcode, String number, String email) throws SQLException { //Creates query to update patient
        String query = "UPDATE MedicalRecords SET FirstName = ?,LastName = ?,Address1 = ?,Address2 = ?,Postcode = ?,PhoneNumber = ?,Email = ?,Sex = ? WHERE NHSNumber = ?";
        Boolean updated = false;
        try {
            PreparedStatement updatePS = conn.prepareStatement(query);
            updatePS.setString(1, first);
            updatePS.setString(2, last);
            updatePS.setString(3, address1);
            updatePS.setString(4, address2);
            updatePS.setString(5, postcode);
            updatePS.setString(6, number);
            updatePS.setString(7, email);
            updatePS.setString(8, gender);
            updatePS.setString(9, nhsID);
            try {
                updatePS.executeUpdate();
            } finally {
                updated = true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(DataAccess.class.getName()).log(Level.SEVERE, null, ex);
        }
        return updated;
    }

    public String getPatients() {
        String rtnString = "SELECT * FROM Patient";
        return rtnString;
    }

    public String getMedicalRecords() {
        String rtnString = "SELECT * FROM MedicalRecords";
        return rtnString;
    }

    public ResultSet searchPatients(String nhsNumber, String lastName, String email, String postcode) {
        ResultSet patientRS = null;
        PreparedStatement patientPS = null;
        String query = "SELECT NHSNumber,FirstName,LastName,Postcode,Email FROM MedicalRecords WHERE NHSNumber = ? OR LastName = ? OR Email = ? OR Postcode = ?";
        try {
            patientPS = conn.prepareStatement(query);
            patientPS.setString(1, nhsNumber);
            patientPS.setString(2, lastName);
            patientPS.setString(3, email);
            patientPS.setString(4, postcode);
            patientRS = patientPS.executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(DataAccess.class.getName()).log(Level.SEVERE, null, ex);
        }
        return patientRS;
    }

    public ResultSet getPreviousResults(String NHSNumber) throws SQLException {

        String previousResults = "SELECT resultID,NHSNumber,Result,Date FROM  Results WHERE NHSNumber = ?";
        PreparedStatement userPS;
        ResultSet userRS = null;
        userPS = conn.prepareStatement(previousResults);
        userPS.setString(1, NHSNumber);
        userRS = userPS.executeQuery();
        return userRS;

    }

    public Boolean addPatientResultToDatabase(String NHSNumber, String result, Date date) {
        Boolean addedResult = false;
        Boolean success = false;
        String insertResult = "INSERT INTO Results"
                + "(NHSNumber,Result,Date) VALUES"
                + "(?,?,?)";
        try {
            PreparedStatement patientPS = conn.prepareStatement(insertResult);
            patientPS.setString(1, NHSNumber);
            patientPS.setString(2, result);
            patientPS.setDate(3, date);
            
            try {
            patientPS.executeUpdate();
            }
            finally {
                success = true;
            }

        } catch (SQLException ex) {
            Logger.getLogger(DataAccess.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (success = true) {
            return true;
        } else {
            return false;
        }
    }
    public Boolean isRegistered(String NHSNumber){
        String sql = "SELECT NHSNumber FROM Patient WHERE NHSNumber = ?";
        Integer size = 0;
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, NHSNumber);
            ResultSet rs = ps.executeQuery();
            rs.last();
            size = rs.getRow();
        } catch (SQLException ex) {
            Logger.getLogger(DataAccess.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return (size>0);
    }
    public Boolean addCSVPatientToDatabase(CSVPatient p){
        Boolean loginCreated = false;
        Boolean medicalRecordCreated = false;
        Date now = new Date(System.currentTimeMillis());
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        cal.add(Calendar.YEAR, -p.getAge());
        Date minusYears = new Date(cal.getTimeInMillis());
        Hasher hasher = new Hasher();
        String sex = "Male";
        String ethnicity = "Other";
        if (p.isFemale()){
            sex = "Female";
        }
        if (p.isBlack()){
            ethnicity = "Black";
        }
        String insertUserSQL = "INSERT INTO Patient"
                + "(NHSNumber,Password,Salt) VALUES"
                + "(?,?,?)";
        String insertMedicalRecordSQL = "INSERT INTO MedicalRecords"
                + "(NHSNumber,FirstName,LastName,Address1,Address2,Postcode,PhoneNumber,Email,DOB,Sex,Ethnicity) VALUES"
                + "(?,?,?,?,?,?,?,?,?,?,?)";
        try{
            
            
            Integer salt = getSalt();
            String password = hasher.getSHA(p.getNhsNumber()+salt.toString());
            PreparedStatement patientPS = conn.prepareStatement(insertUserSQL);
            PreparedStatement medicalRecordPS = conn.prepareStatement(insertMedicalRecordSQL);
            patientPS.setString(1, p.getNhsNumber());
            patientPS.setString(2, password);
            patientPS.setString(3, salt.toString());
            medicalRecordPS.setString(1,p.getNhsNumber());
            medicalRecordPS.setString(2,"Please update");
            medicalRecordPS.setString(3,"Please update");
            medicalRecordPS.setString(4,"Please update");
            medicalRecordPS.setString(5,"Please update");
            medicalRecordPS.setString(6,"SW1 1AA");
            medicalRecordPS.setString(7,"07000123456");
            medicalRecordPS.setString(8,"Please update");
            medicalRecordPS.setDate(9, minusYears);
            medicalRecordPS.setString(10, sex);
            medicalRecordPS.setString(11, ethnicity);
            
            try{
                patientPS.executeUpdate();
            }finally{
                loginCreated = true;
                System.out.println("Login Created");
            }
            try{
                medicalRecordPS.executeUpdate();
            }finally{
                medicalRecordCreated = true;
                System.out.printf("Medical records created");
            }
        }catch (SQLException ex) {
            Logger.getLogger(DataAccess.class.getName()).log(Level.SEVERE, null, ex);
        }
        return (loginCreated && medicalRecordCreated);
    }
    public Boolean addPatientToDatabase(String nhsID, String password,
            String firstName, String lastName, LocalDate dob, String ethnicity,
            String gender, String address1,
            String address2, String postcode, String number, String email, String salt) {
        Boolean loginCreated = false;
        Boolean medicalRecordCreated = false;
        String insertUserSQL = "INSERT INTO Patient"
                + "(NHSNumber,Password,Salt) VALUES"
                + "(?,?,?)";
        String insertMedicalRecordSQL = "INSERT INTO MedicalRecords"
                + "(NHSNumber,FirstName,LastName,Address1,Address2,Postcode,PhoneNumber,Email,DOB,Sex,Ethnicity) VALUES"
                + "(?,?,?,?,?,?,?,?,?,?,?)";
        try {
            PreparedStatement patientPS = conn.prepareStatement(insertUserSQL);
            PreparedStatement medicalRecordPS = conn.prepareStatement(insertMedicalRecordSQL);
            //Arguments for Patient insert
            patientPS.setString(1, nhsID);
            patientPS.setString(2, password);
            patientPS.setString(3, salt);
            //Arguments for medical record insert
            medicalRecordPS.setString(1, nhsID);
            medicalRecordPS.setString(2, firstName);
            medicalRecordPS.setString(3, lastName);
            medicalRecordPS.setString(4, address1);
            medicalRecordPS.setString(5, address2);
            medicalRecordPS.setString(6, postcode);
            medicalRecordPS.setString(7, number);
            medicalRecordPS.setString(8, email);
            medicalRecordPS.setDate(9, Date.valueOf(dob));
            medicalRecordPS.setString(10, gender);
            medicalRecordPS.setString(11, ethnicity);

            try {
                patientPS.executeUpdate();
            } finally {
                loginCreated = true;
            }
            try {
                medicalRecordPS.executeUpdate();
            } finally {
                medicalRecordCreated = true;
            }

        } catch (SQLException ex) {
            Logger.getLogger(DataAccess.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (loginCreated && medicalRecordCreated) {
            return true;
        } else {
            return false;
        }
    }

    public static Boolean loginPatientToDatabase(String NHSNumber, String password) throws SQLException {
        String dbPassHash = null;
        Hasher hasher = new Hasher();
        String salt = new String();
        String getPatientSQL = "SELECT * FROM Patient WHERE NHSNumber = ?";
        try {
            PreparedStatement getPatientPS = conn.prepareStatement(getPatientSQL);
            getPatientPS.setString(1, NHSNumber);
            ResultSet patientRS = getPatientPS.executeQuery();
            if (patientRS.next()) {
                dbPassHash = patientRS.getString("Password");
                salt = patientRS.getString("salt");
            }
            password += salt;

        } catch (SQLException ex) {
            System.out.println(ex);
        }
        password = hasher.getSHA(password);
        if (password.equals(dbPassHash)) {    //screenshot this to show refactoring
            return true;
        } else {
            return false;
        }
    }

    public static Boolean loginClinicianToDatabase(String hcpID, String password) throws SQLException {
        String dbPassHash = null;
        Hasher hasher = new Hasher();
        String salt = new String();
        String getClinicianSQL = "SELECT * FROM Clinician WHERE HCPID = ?";
        try {
            PreparedStatement getClinicianPS = conn.prepareStatement(getClinicianSQL);
            getClinicianPS.setString(1, hcpID);
            ResultSet clinicianRS = getClinicianPS.executeQuery();
            if (clinicianRS.next()) {
                System.out.println("Works");
                dbPassHash = clinicianRS.getString("Password");
                salt = clinicianRS.getString("salt");
            }
            password += salt;
            System.out.println(password);

        } catch (SQLException ex) {
            System.out.println(ex);
        }
        password = hasher.getSHA(password);
        System.out.println(password);
        if (password.equals(dbPassHash)) {    //screenshot this to show refactoring
            return true;
        } else {
            return false;
        }
    }
    public int getSalt(){
        Random rand = new Random();
        int salt = rand.nextInt(999) + 1;
        return salt;
    }

//            try(PreparedStatement loginPS = conn.prepareStatement
//                ("Select NHSNumber, Password " + "from Patient where NHSNumber=? and Password=?")) {            loginPS.setString(1, NHSNumber);
//            loginPS.setString(2, Password);
//            
//            ResultSet RS = loginPS.executeQuery();
//            if ( RS.next()) {
//                System.out.println("Login Succesful");
//                login = true;
//            }
//            else 
//            {
//                System.out.println("Login details Inncorect");
//                login = false;
//            }
//            RS.close();
//            loginPS.close();
//    }
//            catch (SQLException ex){
//                System.out.println(ex);
//            }         
//    public static Boolean loginClinicianToDatabase(String hcpID, String password) throws SQLException {
//        String dbPassHash = null;
//        Hasher hasher = new Hasher();
//        String salt = new String();
//        String getClinicianSQL = "SELECT * FROM Clinician WHERE HCPID = ?";
//        try {
//            PreparedStatement getClinicianPS = conn.prepareStatement(getClinicianSQL);
//            getClinicianPS.setString(1, hcpID);
//            ResultSet ClinicianRS = getClinicianPS.executeQuery();
//            if (ClinicianRS.next()) {
//                dbPassHash = ClinicianRS.getString("Password");
//                salt = ClinicianRS.getString("salt");
//            }
//            password += salt;
//
//        } catch (SQLException ex) {
//            System.out.println(ex);
//        }
//        password = hasher.getSHA(password);
//        if (password.equals(dbPassHash)) {    //screenshot this to show refactoring
//            return true;
//        } else {
//            return false;
//        }
//    }
}
