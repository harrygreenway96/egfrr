package Controller;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import Model.*;

/**
 *
 * @author k1527629
 */
public class Index extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Styles style = new Styles();
        Scripts script = new Scripts();
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession(false);
        String type = "Guest";
        /* DIsplay the top bar to any session attribute of type*/

        try (PrintWriter out = response.getWriter()) {
            out.print("<!DOCTYPE HTML>");
            out.print("<html>");
            out.print("<head>");
            out.print("<meta charset=\"UTF-8\">");
            out.print("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">");
            out.print("<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js\"></script>");
            out.print(style.getStyleForNavBar());
            out.print(style.getStyleForResultTable());


            
            request.getRequestDispatcher("/WEB-INF/navbar.jsp").include(request, response);
            if (session != null) {
                if ((String) session.getAttribute("Type") != null) {
                    type = (String) session.getAttribute("Type");
                }
            }
            out.print("<body>");
            switch (type) {
                case "Patient":
                    request.getRequestDispatcher("/WEB-INF/PatientCalc.jsp").include(request, response);

                    break;
                case "Clinician":
                    out.print(style.getStyleForClinicianPage());
                    request.getRequestDispatcher("/WEB-INF/ClinicianPage.jsp").include(request, response);
                    break;
                case "Guest":
                    out.print(style.getStyleForGuestCalc());

                    request.getRequestDispatcher("/WEB-INF/GuestCalc.jsp").include(request, response);

                    out.print(style.getStyleForResultTable());

                    break;
                default:
                    out.print(style.getStyleForGuestCalc());

                    request.getRequestDispatcher("/WEB-INF/GuestCalc.jsp").include(request, response);

                    out.print(style.getStyleForResultTable());

                    break;
            }
            out.print("</body>");
            out.print("</html>");
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
