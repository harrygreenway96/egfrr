
<%-- 
    Document   : GuestCalc
    Created on : 08-Feb-2019, 16:54:09
    Author     : k1423138
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ page import="Model.*,Controller.*,java.sql.ResultSet;" %>

<%Scripts script = new Scripts();%>
        

<%@ page import="Model.*,Controller.*,java.sql.ResultSet;" %>

        
<%=script.getScriptsForGuestCalc()%>
    </head>
    <body>
        <p>
        <div id="left">
        <H4>Guest Calculator</H4>
        <input type="text" name="Creatine" id="creatinine" placeholder="Creatinine level"></input>
        <br>
        <select id="unit">
            <option value="1">μmol/l</option>
            <option value="0">mg/dl</option>
        </select>
        <br>
                <br>
                <input id="age" type="number" placeholder="Age (18+)" min="18" max="110"/></br>
                <select id="gender">
                    <option value="Female">Female</option>
                    <option value="Male">Male</option>
                </select>
                <br>
                <br>
                <select id="ethnicity">
                    <option value="Asian">Asian/Asian British</option>
                    <option value="Black">Black/Black British</option>
                    <option value="Irish">Gypsy/Traveller/Irish</option>
                    <option value="Mixed">Mixed/Multiple</option>
                    <option value="White">White/White British</option>
                    <option value="Other">Other</option>
                </select> <br>
                <input id="calculate" type="button" value="Calculate"> <br>
            </div>
        <div id="right">
            <br>
            <h1>Background Information </h1>
            <h3>Chronic kidney disease can be discovered using the glomerular filtration rate(eGFR)which is a number based on the patients’ blood test that measure creatinine. Creatinine is a waste product that is normally filtered out through the kidneys, but people suffering from kidney disease will have this product in their blood.</h3>
            <h3>eGFR is an efficient method to uncover kidney disease, but for some people, like those under 18, pregnant, obese or very muscular, the test has proven to be inaccurate.</h3>
            <h3>Stage 1: Normal Function (Slight Damage)- >90</h3>
            <h3>Stage 2: Mild Kidney Damage- 60-89</h3>
            <h3>Stage 3A: Mild to Moderate Kidney Damage- 45-59</h3>
            <h3>Stage 3B: Moderate to Severe Kidney Damage- 30-44</h3>
            <h3>Stage 4: Severely Reduced Kidney Function- 15-29</h3>
            <h3>Stage 5: End-Stage Kidney Failure- <15 </h3>
        </div>
        <br>
        <br>
        <div id="result">
            <h3>Your Result</h3>
            <label id="resultLabel"></label>
            
            <table id="resultTable">
                <tr id="r0">
                    <th>Stage</th>
                    <th>eGFR Value</th>
                    <th>Description</th>
                </tr>
                <tr id="r1">
                    <td>1</td>
                    <td>90+</td>
                    <td>Normal kidney function but urine findings or structural
                        abnormalities or genetic trait point to kidney disease</td>
                </tr>
                <tr id="r2">
                    <td>2</td>
                    <td>60-89</td>
                    <td>Mildly reduced kidney function, and other findings (as
                        for stage 1) point to kidney disease
                    </td>
                </tr>
                <tr id="r3A">
                    <td>3A</td>
                    <td>45-59</td>
                    <td rowspan="2">Moderately reduced kidney function
                    </td>
                </tr>
                <tr id="r3B">
                    <td>3B</td>
                    <td>30-44</td>
                </tr>
                <tr id="r4">
                    <td>4</td>
                    <td>15-29</td>
                    <td>Severely reduced kidney function
                    </td>
                </tr>
                <tr id="r5">
                    <td>5</td>
                    <td><15</td>
                    <td>Very severe, or end stage kidney failure</td>
                </tr>
            </table>
        </div>
        </p>


        <br>

        </form>
</body>
</html>
