
<%@page import="Model.Scripts"%>
<%-- 
    Document   : ClinicianPage
    Created on : 04-Feb-2019, 13:36:06
    Author     : k1527629
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%String path = request.getContextPath();
    Scripts script = new Scripts();
%>
<%=script.getScriptsForClinicianPage()%>
<div id ="title2">
<h4>Clinician Page</h4>
</div>
<%if ((String)request.getParameter("created") != null){
    if (((String)request.getParameter("created")).equals("true")){%>
    <b><span style="color:red">Account Created</br></span></b>
    <%}else if (((String)request.getParameter("created")).equals("false")){%>
    <b><span style="color:red">Account not created, please try again.</br></span></b>
    <%}
    }%>
<div id="search">
<%if ((String)request.getParameter("found") != null){
    if (((String)request.getParameter("found")).equals("false")){%>
    <b><span style="color:red">No matching patients found, please try again.</br></span></b>
    <%}
    }%>
    <%if ((String)request.getParameter("empty") != null){
    if (((String)request.getParameter("empty")).equals("true")){%>
    <b><span style="color:red">Please fill in at least one field and try again.</br></span></b>
    <%}
    }%>
    <p>Fill in any known details below to search for a patient and perform a calculation:</br>
        Alternatively, <span style="color: blue"><label id="showAddNew">Click Here</label></span> to add a new patient.
    </p>
    <form action="<%=path%>/Search" method="POST">
        <table id="searchTable">
            <tr>
                <td>NHS Number:</td>
                <td><input name="nhsID" type="text"/></td>
            </tr>
            <tr>
                <td>Last Name:</td>
                <td><input name="last" type="text"/></td>
            </tr>
            <tr>
                <td>Email:</td>
                <td><input name="email" type="Text"/></td>
            </tr>
            <tr>
                <td>Postcode:</td>
                <td><input name="postcode" type="text"/></td>
            </tr>
            <tr>
                <td></td>
                <td><input value="Search" type="submit"/></td>
            </tr>
        </table>
    </form>
</div>
<div id="newPatient">
    <p>Fill in the patients details below to store their details and create a patient account</br>
        Alternatively, <span style="color: blue"><label id="showSearch">Click Here</label></span> to show the search form.
    </p>
    <form action="<%=path%>/ClinicianAddPatient" method="POST">  
            <table width="200" border="0" align="center">
                <tbody>
                        <tr>
                            <td>
                                <input type="text" name="NHSID" placeholder="NHS ID" id="NHSID">
                            </td>
                            <td>
                                <input type="password" name="password" placeholder="Password" id="password">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="text" name="FirstName" placeholder="First Name" required> 
                            </td>
                            <td>
                                <input type="text" name="LastName" placeholder="Last Name" required> 
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="date" name="date" placeholder="Date of Birth" id="date">
                            </td>
                            <td>

                                <select name="ethnicity" id="eth">
                                    <option value="">Ethnicity</option>
                                    <option value="Asian">Asian/Asian British</option>
                                    <option value="Black">Black/Black British</option>
                                    <option value="Irish">Gypsy/Traveller/Irish</option>
                                    <option value="Mixed">Mixed/Multiple</option>
                                    <option value="Other">Other</option>
                                    <option value="White">White/White British</option>
                                </select> 
                            </td>

                        </tr>
                        <tr>
                            <td>


                                <select name="gender" placeholder="Gender" id="gen">
                                    <option value="">Gender</option>
                                    <option value="Female">Female</option>
                                    <option value="Male">Male</option>
                                </select>

                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="text" name="address1" placeholder="Address Line 1" required>
                            </td>
                            <td>
                                <input type="text" name="address2" placeholder="Address Line 2" required>  
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="text" name="postcode" placeholder="Postcode" required>  
                            </td>

                            <td>

                                <input type="text" name="number" placeholder="Phone Number" id="number"> 
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="email" name="email" placeholder="email" id="email"> 
                            </td>
                            <td>

                                <textarea name="notes" id="notes" placeholder="Notes"></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="submit" value="Sign Up"> 
                            </td>

                        </tr>
                </tbody>
            </table>
        </form>
</div>
<div id="import">
    <b>Have a CSV file? Upload it here!</b>(Accepted format: NHS Number, Gender<1:0,F:M>, Ethnicity<B:O>, Creatinine/μmol)
    <form action="<%=path%>/UploadPatients" method="POST" enctype="multipart/form-data">
        <input type="file" name="file"/>
        <input type="submit"/>
    </form>
</div>
