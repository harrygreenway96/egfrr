<%@page import="Model.Styles"%>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>sign up</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <%Styles style = new Styles();%>
        <%=style.getStyleForSignUp()%>
    </head>
    <body>
        <div id="head">
            <h1>Enter Details to Sign Up</h1>
        </div>
        <div id="sign">
            <form action="PatientSignUp">  
                <table width="200" border="0" align="center">
                    <tbody>
                        <tr>
                            <td>
                                <input type="text" name="NHSID" placeholder="NHS ID" id="NHSID">
                            </td>
                            <td>
                                <input type="password" name="password" placeholder="Password" id="password">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="text" name="FirstName" placeholder="First Name" required> 
                            </td>
                            <td>
                                <input type="text" name="LastName" placeholder="Last Name" required> 
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="date" name="date" placeholder="Date of Birth" id="date">
                            </td>
                            <td>

                                <select name="ethnicity" id="eth">
                                    <option value="">Ethnicity</option>
                                    <option value="Asian">Asian/Asian British</option>
                                    <option value="Black">Black/Black British</option>
                                    <option value="Irish">Gypsy/Traveller/Irish</option>
                                    <option value="Mixed">Mixed/Multiple</option>
                                    <option value="Other">Other</option>
                                    <option value="White">White/White British</option>
                                </select> 
                            </td>

                        </tr>
                        <tr>
                            <td>


                                <select name="gender" placeholder="Gender" id="gen">
                                    <option value="">Gender</option>
                                    <option value="Female">Female</option>
                                    <option value="Male">Male</option>
                                </select>

                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="text" name="address1" placeholder="Address Line 1" required>
                            </td>
                            <td>
                                <input type="text" name="address2" placeholder="Address Line 2" required>  
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="text" name="postcode" placeholder="Postcode" required>  
                            </td>

                            <td>

                                <input type="text" name="number" placeholder="Phone Number" id="number"> 
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="email" name="email" placeholder="email" id="email"> 
                            </td>
                            <td>

                                <textarea name="notes" id="notes" placeholder="Notes"></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="submit" value="Sign Up"> 
                            </td>

                        </tr>
                    </tbody>
                </table>
            </form>
        </div>
    </body>
</html>
