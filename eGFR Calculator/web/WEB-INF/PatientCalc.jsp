<%-- 
    Document   : PatientCalc
    Created on : 08-Feb-2019, 16:53:26
    Author     : k1423138
--%>
<!--For some reason I can't link to styles or scripts from external files-->


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="Model.*,Controller.*,java.sql.ResultSet,java.sql.SQLException" %>
<% PatientFactory pf = new PatientFactory();
    String workingUserID = (String) session.getAttribute("WorkingUser");
    String path = request.getContextPath();
    DataAccess da = new DataAccess();
    ResultSet patientRS = null;
    Patient workingUser = null;

    da.openConnection();
    patientRS = da.getWorkingUser(workingUserID);
    workingUser = pf.createPatientFromResultSet(patientRS);
    Scripts script = new Scripts();
    String age = Integer.toString(workingUser.getAge());
    String female = Boolean.toString(workingUser.isFemale());
    String black = Boolean.toString(workingUser.isBlack());
    
    Styles style = new Styles();
%>
   
<!DOCTYPE html>

        
            <%=script.getScriptsForPatientCalc(age, female, black)%>
            <%=style.getStyleForPatientCalc()%>
        
    </head>
        <br>
        <div id="patC">
        <h3>Patient Calculator</h3>
        </div>
        <div id ="box">
        <div id="calc">  
            <p>
                <input type="Text" name ="CreatineValue" placeholder="Creatine Value" id="CreatinineValue">
                </br>
                <select id="unit">
                    <option value="1">μmol/l</option>
                    <option value="0">mg/dl</option>
                </select>

            </p>
            <br>
            <input type="Submit" id="calculate" value="Calculate"> <br>
        </div>
        <div id="info">
            <br>
            <h1>What to do next...</h1>
            <h3>There is no cure for kidney disease, but it may be possible to stop its progress or at least slow down the damage.</h3>
            <h3>You can take the following steps depending on your results:</h3>
            <h3>Stage 1: Eat a healthy diet, exercise regularly and stop smoking and have regular checkups with the doctor</h3>
            <h3>Stage 2: Keep blood sugar or diabetes under control, exercise regularly, healthy diet</h3>
            <h3>Stage 3A/3B: Healthy diet, regularly take prescribed medications, exercise regularly</h3>
            <h3>Stage 4: Regularly visit your doctor, carry out necessary tests. Learn about different treatment options, eat healthy and exercise</h3>
            <h3>Stage 5: Change diet, start treatment or look into different options e.g. kidney transplant</h3>
        </div>
        </div>

        <div id="result">
            <br>
            <h3 id="resultHeader"><%=workingUser.getFirstName()%>'s Result</h3>
            <label id="resultLabel"></label>
            <table id="resultTable">
                <br>
                <tr id="r0">
                    <th>Stage</th>
                    <th>eGFR Value</th>
                    <th>Description</th>
                </tr>
                <tr id="r1">
                    <td>1</td>
                    <td>90+</td>
                    <td>Normal kidney function but urine findings or structural
                        abnormalities or genetic trait point to kidney disease</td>
                </tr>
                <tr id="r2">
                    <td>2</td>
                    <td>60-89</td>
                    <td>Mildly reduced kidney function, and other findings (as
                        for stage 1) point to kidney disease
                    </td>
                </tr>
                <tr id="r3A">
                    <td>3A</td>
                    <td>45-59</td>
                    <td rowspan="2">Moderately reduced kidney function
                    </td>
                </tr>
                <tr id="r3B">
                    <td>3B</td>
                    <td>30-44</td>
                </tr>
                <tr id="r4">
                    <td>4</td>
                    <td>15-29</td>
                    <td>Severely reduced kidney function
                    </td>
                </tr>
                <tr id="r5">
                    <td>5</td>
                    <td><15</td>
                    <td>Very severe, or endstage kidney failure</td>
                </tr>
               
                <script>
function SetHiddenValue()
{
    document.getElementById("X").value = document.getElementById("resultLabel").innerHTML;
    var value2 = document.getElementById("resultLabel").innerHTML;
    var hidden = document.getElementById("hidden").value;
    hidden = value2;
}

</script>
            </table>
            
            <form action ="<%=path%>/SavePatientResults" onSubmit="SetHiddenValue()" method="POST">
    <input type="hidden" name="X" id="X" />
 <input  type="submit" value="Test">
</form>
        </div>
            
<%da.closeConnection();%>