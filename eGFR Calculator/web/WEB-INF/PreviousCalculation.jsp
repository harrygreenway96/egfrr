<%-- 
    Document   : PreviousCalculation
    Created on : 04-Feb-2019, 13:51:04
    Author     : k1527629
--%>

<%@page import="java.util.GregorianCalendar"%>
<%@page import="java.util.Calendar"%>

<%@page import="java.text.SimpleDateFormat"%>
<%@page import="org.jfree.data.time.TimeSeriesCollection"%>
<%@page import="org.jfree.data.time.TimeSeries"%>
<%@page import="org.jfree.data.time.Day"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.logging.Level"%>
<%@page import="java.io.*;"%>
<%@page import="org.jfree.chart.JFreeChart; "%>
<%@page import="org.jfree.chart.ChartFactory; "%>
<%@page import="org.jfree.chart.ChartUtilities;"%>
<%@page import="org.jfree.chart.plot.PlotOrientation;"%>
<%@page import="org.jfree.data.category.DefaultCategoryDataset;"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="Model.*,Controller.*,java.sql.ResultSet,java.sql.SQLException" %>

    <body>
         <div id="container">
         <div id="resNo">
            <% if ((String)request.getParameter("saved") != null){
               if (((String)request.getParameter("saved")).equals("true")){%>
                <b><span style="color:red">Result Saved</br></span></b>
                <%}}%>
            <<% PatientFactory pf = new PatientFactory();
                String userID = (String) session.getAttribute("ID");
                String path = request.getContextPath();
                DataAccess da = new DataAccess();
                ResultSet userRS = null;
                  TimeSeries series = new TimeSeries("time series", Day.class);



                da.openConnection();
                        String NHSNumber = (String) session.getAttribute("WorkingUser");

                        if (NHSNumber != null) {

                            ResultSet patientRS = da.getPreviousResults(NHSNumber);
                            while (patientRS.next()) {
                                            String nhs = patientRS.getString("NHSNumber");
                                            String resultID = patientRS.getString("resultID");
                                            String result = patientRS.getString("Result");
                                            String datee = patientRS.getString("Date"); 




                            }
                            patientRS.beforeFirst();
                            while (patientRS.next()) {
                                            String id = patientRS.getString("NHSNumber");
                                            String resultID = patientRS.getString("resultID");
                                            String result = patientRS.getString("Result");
                                            String datee = patientRS.getString("Date"); 

                                            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

                                            Date date = formatter.parse(datee);


                                            Integer resultInt = Integer.parseInt(result);
                                           series.add(new Day(date), resultInt);
                                           TimeSeriesCollection datasett = new TimeSeriesCollection();
                                           datasett.addSeries(series);
                                           JFreeChart timechart = ChartFactory.createTimeSeriesChart("Results", "Date", "Result", datasett, true, false, true);
                                         String filename ="H:/Data/NetBeansProjects/TeamBeta/timechart.jpg"; 
                                         ChartUtilities.saveChartAsJPEG(new File(filename), timechart, 500, 300);



                            if (patientRS != null) {%>
                                <table id=\"PreviousCalculations\">
                                <tr>
                                    <td><label>NHS Number</label></td>
                                    <td><%=id%></td>
                                </tr>
                                <tr>
                                    <td><label>Result ID</label></td>
                                    <td><%=resultID%></td>
                                </tr>
                                <tr>
                                    <td><label>Result</label></td>
                                    <td><%=result%></td>
                                </tr>
                                <tr>
                                    <td><label>Date</label></td>
                                    <td><%=date%></td>
                                </tr>



                          <%  } }

                                            }da.closeConnection(); %>
         
         <input type="button" value="Create Chart"/>
         </div>
         <div id="chart"> </div>
         </div>
    </body>
</html>