<%-- 
    Document   : PatientProfile
    Created on : 18-Feb-2019, 18:46:39
    Author     : k1423138
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="Model.*,Controller.*,java.sql.ResultSet,java.sql.SQLException" %>
<% PatientFactory pf = new PatientFactory();
    String userID = (String) session.getAttribute("ID");
    String path = request.getContextPath();
    DataAccess da = new DataAccess();
    ResultSet userRS = null;
    Patient user = null;

    da.openConnection();
    userRS = da.getWorkingUser(userID);
    user = pf.createPatientFromResultSet(userRS);


%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><%=user.getFirstName()%>'s Profile - <%=user.getNhsNumber()%></title>
    </head>
    <body>
        <div id="content">
            <form id="updateInfo" action="UpdateInfo" method="POST">
                <table id="user">
                    <tr>
                        <td><label>First Name</label></td>
                        <td><input name="first" type="text" value="<%=user.getFirstName()%>"/></td>
                    </tr>
                    <tr>
                        <td><label>Last Name</label></td>
                        <td><input name="last" type="text" value="<%=user.getLastName()%>"/></td>  
                    </tr>
                    <tr>
                        <td><label>Address Line 1</label></td>
                        <td><input name="address1" type="text" value="<%=user.getAddress1()%>"/></td>
                    </tr>
                    <tr>
                        <td><label>Address Line 2</label></td>
                        <td><input name="address2" type="text" value="<%=user.getAddress2()%>"/></td>
                    </tr>
                    <tr>
                        <td><label>Postcode</label></td>
                        <td><input name="postcode" type="text" value="<%=user.getPostCode()%>"/></td>
                    </tr>
                    <tr>
                        <td><label>Telephone Number</label></td>
                        <td><input name="number" type="text" value="<%=user.getPhoneNumber()%>"/></td>
                    </tr>
                    <tr>
                        <td><label>Gender</label></td>
                        <td><select name="gender">
                                <%if (user.isFemale()){%>
                                    <option value="Female">Female</option>
                                    <option value="Male">Male</option>
                                <%}else{%>
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                    <%}%>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td><label>Ethnicity</label></td>
                        <td><%=user.getEthnicity()%></td>
                    </tr>
                    <tr>
                        <td><label>E-mail</label></td>
                        <td><input name="email" type="email" value="<%=user.getEmail()%>"/></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><input type="submit" value="Update"/></td>
                    </tr>
                </table>
            </form>
        </div>
    </body>
</html>
<%da.closeConnection();%>
